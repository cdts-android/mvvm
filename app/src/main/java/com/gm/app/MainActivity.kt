package com.gm.app

import com.gm.app.databinding.ActivityMainBinding
import com.mvvm.base.lib.activity.BaseActivity

class MainActivity : BaseActivity<ActivityMainBinding,MainViewModel>() {

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun bindViewModel() {
        mBinding.viewModel = mViewModel
    }

}