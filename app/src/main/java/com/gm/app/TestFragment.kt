package com.gm.app

import android.os.Bundle
import android.view.View
import androidx.navigation.Navigation
import com.gm.app.databinding.FragmentTestBinding
import com.mvvm.base.lib.fragment.BaseFragment

class TestFragment : BaseFragment<FragmentTestBinding, TestViewModel>(){


    override fun getLayoutId(): Int = R.layout.fragment_test

    override fun bindViewModel() {
        mBinding.viewModel = mViewModel
    }

    override fun onClickResponse(view: View, bundle: Bundle?) {
        if (view.id == R.id.te2){
            navigate(R.id.testStateFragment)
        }
    }

}