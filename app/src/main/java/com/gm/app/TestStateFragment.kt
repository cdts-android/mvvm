package com.gm.app

import android.os.Bundle
import android.view.View
import com.gm.app.databinding.FragmentTestStateBinding
import com.mvvm.base.lib.fragment.BaseStateFragment

class TestStateFragment : BaseStateFragment<FragmentTestStateBinding, TestStateViewModel>() {

    override fun getLayoutId(): Int = R.layout.fragment_test_state

    override fun bindViewModel() {
        mBinding.viewModel = mViewModel
    }

    override fun onBindComplete() {
        super.onBindComplete()
        mViewModel.initData()
    }

    override fun onClickResponse(view: View, bundle: Bundle?) {
        popBackStack()
    }

}