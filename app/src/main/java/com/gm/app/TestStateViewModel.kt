package com.gm.app

import android.util.Log
import android.view.View
import com.gm.app.api.AccountService
import com.gm.app.api.IccountService
import com.gm.repository.net.base.ApiCode
import com.gm.repository.net.base.ApiException
import com.gm.repository.net.base.BaseData
import com.mvvm.base.lib.util.ApiUtils
import com.mvvm.base.lib.util.RxObserver
import com.mvvm.base.lib.util.bindStatus
import com.mvvm.base.lib.viewmodel.BaseStateViewModel

class TestStateViewModel : BaseStateViewModel() {

    private val mAccountService: IccountService by lazy {
        AccountService()
    }

    override fun handleClickEvent(view: View, vararg args: Any?) {
        super.handleClickEvent(view, *args)
        notifyClickResponseHandlers(view,null)
    }

    fun initData() {
//        mAccountService.getBus2logoutShop()
//            .bindStatus(this, getMultipleStatusView())
//            .subscribe({
//
//            },{
//            }).addTo(mCompositeDisposable)
//        showLoadingView(true)
//        initData3()
    }

    fun initData2(){
        mAccountService.getBus2logoutShop()
            .bindStatus(this, getMultipleStatusView())
            .subscribe(RxObserver<BaseData<Any>>(
                onSubscribe = {mCompositeDisposable.add(it)},
                onNext = {

                },
            ))
    }

    fun initData3(){
        mAccountService.getBus2logoutShop()
            .subscribe(RxObserver<BaseData<Any>>(
                onNext = {

                },
                onError = {
                    showToast(ApiUtils.getErrMessage((it as ApiException).code))
                }
            ))
    }

    override fun onViewVisible() {
        super.onViewVisible()
        showToast(R.string.app_name)
    }

    override fun onViewHidden() {
        super.onViewHidden()
    }
}