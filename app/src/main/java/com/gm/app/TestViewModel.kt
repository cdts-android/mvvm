package com.gm.app

import android.view.View
import com.gm.app.sp.AccountSpService
import com.gm.app.sp.IccountSpService
import com.mvvm.base.lib.viewmodel.BaseViewModel

class TestViewModel:BaseViewModel() {

    private val mAccountSpService: IccountSpService by lazy {
        AccountSpService()
    }

    override fun handleClickEvent(view: View, vararg args: Any?) {
        super.handleClickEvent(view, *args)
        notifyClickResponseHandlers(view,null)
    }

    override fun onViewVisible() {
        super.onViewVisible()
    }

    fun insetToken(){
        mAccountSpService.insertToken("test")
    }

    fun getToken(){
        mAccountSpService.getToken()
    }

}