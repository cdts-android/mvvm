package com.gm.app.api

import com.gm.repository.net.NetMgr
import com.gm.repository.net.base.BaseData
import com.mvvm.base.lib.util.async
import io.reactivex.Observable

class AccountService : IccountService {

    override fun getBus2logoutShop(): Observable<BaseData<Any>> {
        return NetMgr.getRetrofit().create(IAccountApi::class.java).getBus2logoutShop().async()
    }

}
