package com.gm.app.api

import com.gm.repository.net.base.BaseData
import io.reactivex.Observable
import retrofit2.http.POST

interface IAccountApi {

    @POST("test")
    fun getBus2logoutShop(): Observable<BaseData<Any>>
}
