package com.gm.app.api

import com.gm.repository.net.base.BaseData
import io.reactivex.Observable

interface IccountService {

    fun getBus2logoutShop(): Observable<BaseData<Any>>
}
