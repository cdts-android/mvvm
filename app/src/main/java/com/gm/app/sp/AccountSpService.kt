package com.gm.app.sp

import com.gm.repository.sp.KitContext
import com.gm.repository.sp.restful.Restful
import com.gm.repository.sp.restful.injectSp
import com.gm.repository.sp.restful.tryException
import com.gm.repository.sp.sp.SpProxy

class AccountSpService : IccountSpService {
    private val mGlobalSp =
        Restful.Builder().injectSp().build().create(IAccountSp::class.java)

    private fun getProxy() = SpProxy(KitContext.instance.getSp())

    override fun getToken(): String {
        return tryException("") { mGlobalSp.getToken(getProxy()) }!!
    }

    override fun insertToken(token: String) {
        tryException {
            mGlobalSp.insertToken(token, getProxy())
        }
    }


}