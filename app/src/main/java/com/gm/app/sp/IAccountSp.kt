package com.gm.app.sp

import com.gm.repository.sp.GlobalSp
import com.gm.repository.sp.sp.*
import java.lang.reflect.Type
import kotlin.jvm.Throws

interface IAccountSp {

    @SpQuery(key = GlobalSp.TOKEN, default = "")
    @Throws(Exception::class)
    fun getToken(@SpRefer proxy: SpProxy, @SpType type: Type = String::class.java): String

    @SpInsert(key = GlobalSp.TOKEN)
    @Throws(Exception::class)
    fun insertToken(@SpValue value: String, @SpRefer proxy: SpProxy, @SpType type: Type = Boolean::class.java): Boolean
}
