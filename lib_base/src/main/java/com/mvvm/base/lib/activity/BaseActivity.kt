package com.mvvm.base.lib.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.NewInstanceFactory
import androidx.navigation.NavController
import androidx.navigation.NavController.OnDestinationChangedListener
import androidx.navigation.NavDestination
import androidx.navigation.Navigation
import com.mvvm.base.lib.R
import com.mvvm.base.lib.fragment.LoadingDialogFragment
import com.mvvm.base.lib.listener.ViewBehavior
import com.mvvm.base.lib.util.LogUtils
import com.mvvm.base.lib.util.ToastUtil
import com.mvvm.base.lib.viewmodel.BaseViewModel
import java.lang.reflect.ParameterizedType

abstract class BaseActivity<D : ViewDataBinding, V : BaseViewModel> : FragmentActivity(),ViewBehavior,
    OnDestinationChangedListener {

    private val TAG = BaseActivity::class.java.simpleName
    protected lateinit var mBinding: D
    protected lateinit var mViewModel: V
    private var mLoadingDialog: LoadingDialogFragment? = null
    protected var mNavController: NavController? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Before data binding, invoke onBindStart callback
        onBindStart()

        mBinding = DataBindingUtil.setContentView(this, getLayoutId())
        //Fetch the view model
        val rawType = (javaClass
            .genericSuperclass as ParameterizedType).actualTypeArguments[1] as Class<V>
        mViewModel = ViewModelProvider(this, NewInstanceFactory())[rawType]
        bindViewModel()
        mBinding.lifecycleOwner = this
        registerInternalObserver()
        onBindComplete()
    }

    abstract fun getLayoutId(): Int

    abstract fun bindViewModel()

    protected open fun onBindStart() {}

    protected open fun onBindComplete() {}

    private fun registerInternalObserver() {
        mViewModel.getLoadingEvent().observe(this) { show: Boolean ->
            showLoadingView(show)
        }
        mViewModel.getToastEvent().observe(this){toasts->
            toasts.content?.let {
                showToast(it)
                return@observe
            }
            toasts.resId?.let {
                showToast(it)
            }
        }
        lifecycle.addObserver((mViewModel as LifecycleEventObserver))
    }


    override fun onDestroy() {
        lifecycle.removeObserver((mViewModel as LifecycleObserver))
        super.onDestroy()
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val focusView = currentFocus
            if (focusView is EditText) {
                val outRect = Rect()
                focusView.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    focusView.clearFocus()
                    val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(focusView.getWindowToken(), 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }

    override fun showLoadingView(showView: Boolean) {
        if (mLoadingDialog == null) {
            mLoadingDialog = LoadingDialogFragment()
        }
        try {
            if (showView) {
                if (isFinishing && !mLoadingDialog!!.isAdded) {
                    mLoadingDialog!!.show(this)
                }
            } else {
                if (isFinishing) {
                    mLoadingDialog!!.dismissAllowingStateLoss()
                }
            }
        } catch (exception: Exception) {
            LogUtils.w(TAG, "Show loading dialog error:" + exception.message)
        }
    }

    override fun showToast(content: String) {
        runOnUiThread {
            ToastUtil.toastShortMessage(content,this.applicationContext)
        }
    }

    override fun showToast(resId: Int) {
        runOnUiThread {
            ToastUtil.toastShortMessage(resId,this.applicationContext)
        }
    }

    protected open fun startActivity(clazz: Class<out Activity?>?) {
        startActivity(Intent(this, clazz))
    }

    protected open fun startActivity(action: String?) {
        startActivity(Intent(action))
    }

    protected open fun startActivity(clazz: Class<out Activity?>?, extras: Bundle?) {
        val intent = Intent(this, clazz)
        intent.putExtras(extras!!)
        startActivity(intent)
    }

    /**
     * Create Navigation controller.
     *
     * @return NavController.
     */
    protected open fun createNavController(): NavController {
        val controller = Navigation.findNavController(this, R.id.fragment_container)
        controller.addOnDestinationChangedListener(this)
        return controller
    }

    /**
     * Navigate to a destination from the current navigation graph.
     *
     * @param id Navigation id
     */
    protected open fun navigate(id: Int) {
        if (mNavController == null) {
            mNavController = createNavController()
        }
        try {
            mNavController?.navigate(id)
        } catch (exception: java.lang.Exception) {
            LogUtils.w(TAG, "Navigate error:" + exception.message)
            onNavigateFailed(mNavController)
        }
    }

    /**
     * Navigate to a destination from the current navigation graph.
     *
     * @param id   Navigation id
     * @param args Arguments
     */
    protected open fun navigate(id: Int, args: Bundle?) {
        if (mNavController == null) {
            mNavController = createNavController()
        }
        try {
            mNavController?.navigate(id, args)
        } catch (exception: java.lang.Exception) {
            LogUtils.w(TAG, "Navigate error:" + exception.message)
            onNavigateFailed(mNavController)
        }
    }

    /**
     * Attempts to pop the controller's back stack.
     *
     * @return true if the stack was popped and the user has been navigated to another destination,
     * false otherwise.
     */
    protected open fun popBackStack(): Boolean {
        if (mNavController == null) {
            mNavController = createNavController()
        }
        try {
            return mNavController?.popBackStack()!!
        } catch (exception: java.lang.Exception) {
            LogUtils.w(TAG, "Navigate error:" + exception.message)
            onNavigateFailed(mNavController)
        }
        return false
    }

    /**
     * Attempts to pop the controller's back stack back to a specific destination.
     *
     * @param destinationId The topmost destination to retain.
     * @param inclusive     Whether the given destination should also be popped.
     * @return true if the stack was popped at least once and the user has been navigated
     * to another destination, false otherwise.
     */
    protected open fun popBackStack(destinationId: Int, inclusive: Boolean): Boolean {
        if (mNavController == null) {
            mNavController = createNavController()
        }
        try {
            return mNavController?.popBackStack(destinationId, inclusive)!!
        } catch (exception: java.lang.Exception) {
            LogUtils.w(TAG, "Navigate error:" + exception.message)
            onNavigateFailed(mNavController)
        }
        return false
    }

    /**
     * Judge whether the fragment is at the top level.
     *
     * @param destId The dest fragment id.
     * @return true if the fragment is at the top level, false otherwise.
     */
    protected open fun isTopFragment(destId: Int): Boolean {
        if (mNavController == null) {
            mNavController = createNavController()
        }
        val navDestination: NavDestination = mNavController?.currentDestination ?: return false
        return navDestination.id == destId
    }

    /**
     * Called when navigate failed.
     */
    protected open fun onNavigateFailed(navController: NavController?) {}

    override fun onDestinationChanged(
        controller: NavController,
        destination: NavDestination,
        arguments: Bundle?
    ) {
        LogUtils.i(TAG, "destination-->" + destination.id + "|" + destination.label)
    }
}