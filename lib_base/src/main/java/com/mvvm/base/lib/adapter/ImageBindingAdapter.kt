package com.mvvm.base.lib.adapter

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.mvvm.base.lib.util.GlideCircleTransform

object ImageBindingAdapter {
    @BindingAdapter("imageUrl")
    fun loadImage(imageView: ImageView, bitmap: Bitmap?) {
        Glide.with(imageView).load(bitmap).into(imageView)
    }

    @BindingAdapter("imageUrl")
    fun loadImage(imageView: ImageView, imageUrl: String?) {
        Glide.with(imageView).load(imageUrl).into(imageView)
    }

    @BindingAdapter("imageUrl")
    fun loadImage(imageView: ImageView, resId: Int) {
        Glide.with(imageView).load(resId).into(imageView)
    }

    @BindingAdapter("imageUrl")
    fun loadImage(imageView: ImageView, drawable: Drawable?) {
        Glide.with(imageView).load(drawable).into(imageView)
    }

    @BindingAdapter(value = ["imageUrl", "placeholder"])
    fun loadImageWithPlaceholder(view: ImageView, url: String?, placeholder: Drawable?) {
        val options: RequestOptions = RequestOptions().placeholder(placeholder).error(placeholder)
        Glide.with(view).load(url).apply(options).into(view)
    }

    @BindingAdapter(value = ["imageUrl", "placeholder", "error"])
    fun loadImage(view: ImageView, url: String?, placeholder: Drawable?, error: Drawable?) {
        val options: RequestOptions = RequestOptions().placeholder(placeholder).error(error)
        Glide.with(view).load(url).apply(options).into(view)
    }

    @BindingAdapter(value = ["imageUrl", "placeholder", "error"])
    fun loadImage(view: ImageView, url: String?, placeholder: Int, error: Drawable?) {
        val options: RequestOptions = RequestOptions().placeholder(placeholder).error(error)
        Glide.with(view).load(url).apply(options).into(view)
    }

    /**
     * Load image with placeholder resId and error resId.
     *
     * @param view        imageView
     * @param url         imageUrl
     * @param placeholder placeholder resId
     * @param error       error resId
     */
    @BindingAdapter(value = ["imageUrl", "placeholder", "error"])
    fun loadImage(view: ImageView, url: String?, placeholder: Int, error: Int) {
        val options: RequestOptions = RequestOptions().placeholder(placeholder).error(error)
        Glide.with(view).load(url).apply(options).into(view)
    }

    /**
     * Load image with circle crop.
     *
     * @param view           the target imageview
     * @param circleImageUrl the image url
     * @param placeholder    the placeholder image
     * @param error          the error image
     */
    @BindingAdapter(value = ["circleImageUrl", "placeholder", "error"])
    fun loadCircleImage(
        view: ImageView,
        circleImageUrl: String?,
        placeholder: Drawable?,
        error: Drawable?
    ) {
        val options: RequestOptions = RequestOptions()
            .placeholder(placeholder)
            .skipMemoryCache(false)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .apply(RequestOptions().circleCrop())
            .dontAnimate()
            .error(error)
        Glide.with(view).load(circleImageUrl).apply(options).into(view)
    }

    /**
     * load circle image.
     */
    @BindingAdapter(value = ["circleImageUrl"])
    fun loadCircleImage(view: ImageView, url: String?) {
        val options: RequestOptions = RequestOptions()
            .transform(GlideCircleTransform())
        Glide.with(view).load(url).apply(options).into(view)
    }
}