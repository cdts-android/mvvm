package com.mvvm.base.lib.application

import android.app.Application

open class BaseApplication :Application(){

    override fun onCreate() {
        super.onCreate()
        initTheme()
    }

    open fun initTheme(){

    }
}