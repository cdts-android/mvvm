package com.mvvm.base.lib.data

data class Toasts(
    val content: String? = null,
    val resId: Int? = null)