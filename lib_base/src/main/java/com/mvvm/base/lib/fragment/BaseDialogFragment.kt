package com.mvvm.base.lib.fragment

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.mvvm.base.lib.R

abstract class BaseDialogFragment<D : ViewDataBinding> : AppCompatDialogFragment() {

    protected var mBinding: D? = null
    private var mWidth = ViewGroup.LayoutParams.WRAP_CONTENT
    private var mHeight = ViewGroup.LayoutParams.WRAP_CONTENT
    private var mGravity = Gravity.CENTER
    private var mAnimRes = -1
    private var mDimAmount = 0.5f
    private var mAlpha = 1f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
            if (getDialogStyle() != -1) getDialogStyle() else STYLE_NO_TITLE,
            if (getDialogTheme() != -1) getDialogTheme() else R.style.Theme_CommonDialog
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (mBinding != null) {
            return mBinding!!.root
        }
        mBinding = DataBindingUtil.inflate(inflater, setLayoutId(), container, false)
        onBindComplete()
        return mBinding!!.root
    }

    override fun onStart() {
        super.onStart()
        updateAttributes()
    }

    override fun onDestroy() {
        super.onDestroy()
        mBinding = null
    }

    open fun setWidth(width: Int): BaseDialogFragment<D>? {
        mWidth = width
        return this
    }

    open fun setHeight(height: Int): BaseDialogFragment<D>? {
        mHeight = height
        return this
    }

    open fun setGravity(gravity: Int): BaseDialogFragment<D>? {
        mGravity = gravity
        return this
    }

    open fun setAnimRes(animRes: Int): BaseDialogFragment<D>? {
        mAnimRes = animRes
        return this
    }

    open fun setDimAmount(dimAmount: Float): BaseDialogFragment<D>? {
        mDimAmount = dimAmount
        return this
    }

    open fun setAlpha(alpha: Float): BaseDialogFragment<D>? {
        mAlpha = alpha
        return this
    }

    open fun isShowing(): Boolean {
        return isAdded && dialog != null && dialog!!.isShowing
    }

    open fun show(activity: FragmentActivity) {
        show(activity, null)
    }

    open fun show(activity: FragmentActivity, tag: String?) {
        show(
            activity.supportFragmentManager,
            tag ?: this.javaClass.simpleName
        )
    }

    open fun show(fragment: Fragment) {
        show(fragment, null)
    }

    open fun show(fragment: Fragment, tag: String?) {
        show(
            fragment.parentFragmentManager,
            tag ?: this.javaClass.simpleName
        )
    }


    protected open fun getDialogStyle(): Int = -1

    protected open fun getDialogTheme(): Int = -1

    abstract fun setLayoutId(): Int

    protected open fun onBindComplete() {}

    private fun updateAttributes() {
        if (dialog != null) {
            val window = dialog!!.window
            val params = window!!.attributes
            params.width = mWidth
            params.height = mHeight
            params.gravity = mGravity
            params.windowAnimations = mAnimRes
            params.dimAmount = mDimAmount
            params.alpha = mAlpha
            window.attributes = params
        }
    }
}