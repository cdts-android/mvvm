package com.mvvm.base.lib.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProvider.NewInstanceFactory
import androidx.navigation.NavController
import androidx.navigation.Navigation
import com.mvvm.base.lib.R
import com.mvvm.base.lib.data.Toasts
import com.mvvm.base.lib.listener.EventResponseHandler
import com.mvvm.base.lib.listener.ViewBehavior
import com.mvvm.base.lib.util.LogUtils
import com.mvvm.base.lib.util.ToastUtil
import com.mvvm.base.lib.viewmodel.BaseViewModel
import java.lang.reflect.ParameterizedType

abstract class BaseFragment<D : ViewDataBinding, V : BaseViewModel> : Fragment(), ViewBehavior,
    EventResponseHandler {

    private val TAG = BaseFragment::class.java.simpleName

    open lateinit var mBinding: D
    open lateinit var mViewModel: V
    private var mLoadingDialog: LoadingDialogFragment? = null
    open var rootView: View? = null
    open var mNavController: NavController? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (rootView != null) {
            return rootView
        }
        mBinding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        //Fetch the view model
        val rawType: Class<V> = (javaClass
            .genericSuperclass as ParameterizedType).actualTypeArguments[1] as Class<V>
        mViewModel = ViewModelProvider(
            if (shareParentData()) requireActivity() else this, NewInstanceFactory()
        ).get(rawType)
        //Bind ViewMode to DataBinding
        bindViewModel()
        rootView = onBindStateView(mBinding)
        //After complete data binding, invoke onBindComplete callback
        onBindComplete()
        registerInternalObserver()
        return rootView
    }

    override fun onStart() {
        super.onStart()
        mViewModel.registerEventListener(this)
    }

    override fun onStop() {
        super.onStop()
        mViewModel.unregisterEventListener(this)
    }


    abstract fun getLayoutId(): Int

    abstract fun bindViewModel()

    open fun onBindStart() {}

    open fun onBindComplete() {}

    open fun onBindStateView(mBinding: D): View? {
        return mBinding.root
    }

    open fun shareParentData(): Boolean {
        return false
    }

    open fun startActivity(clazz: Class<out Activity?>) {
        startActivity(Intent(requireContext(), clazz))
    }

    open fun startActivity(action: String) {
        startActivity(Intent(action))
    }

    open fun startActivity(clazz: Class<out Activity?>, extras: Bundle) {
        val intent = Intent(requireContext(), clazz)
        intent.putExtras(extras)
        startActivity(intent)
    }

    override fun onClickResponse(view: View, bundle: Bundle?) {}

    override fun onEventResponse(event: Int, bundle: Bundle?) {}

    override fun showLoadingView(showView: Boolean) {
        if (mLoadingDialog == null) {
            mLoadingDialog = LoadingDialogFragment()
        }
        try {
            if (showView) {
                if (isVisible && !mLoadingDialog!!.isAdded()) {
                    mLoadingDialog!!.show(this)
                }
            } else {
                mLoadingDialog!!.dismissAllowingStateLoss()
            }
        } catch (e: Exception) {
            LogUtils.w(TAG, "Show loading dialog error:" + e.message)
        }
    }

    override fun showToast(content: String) {
        requireActivity().runOnUiThread {
            ToastUtil.toastShortMessage(content,this.requireActivity().applicationContext)
        }
    }

    override fun showToast(resId: Int) {
        requireActivity().runOnUiThread {
            ToastUtil.toastShortMessage(resId,this.requireActivity().applicationContext)
        }
    }

    protected open fun createNavController(): NavController {
        return Navigation.findNavController(requireActivity(), R.id.fragment_container)
    }

    /**
     * Navigate to a destination from the current navigation graph.
     *
     * @param id Navigation id
     */
    protected open fun navigate(id: Int) {
        if (mNavController == null) {
            mNavController = createNavController()
        }
        try {
            mNavController?.navigate(id)
        } catch (e: java.lang.Exception) {
            LogUtils.w(TAG, "Navigate error:" + e.message)
            onNavigateFailed(mNavController)
        }
    }

    /**
     * Navigate to a destination from the current navigation graph.
     *
     * @param id   Navigation id
     * @param args Arguments
     */
    protected open fun navigate(id: Int, args: Bundle?) {
        if (mNavController == null) {
            mNavController = createNavController()
        }
        try {
            mNavController?.navigate(id, args)
        } catch (e: java.lang.Exception) {
            LogUtils.w(TAG, "Navigate error:" + e.message)
            onNavigateFailed(mNavController)
        }
    }

    /**
     * Attempts to pop the controller's back stack.
     *
     * @return true if the stack was popped and the user has been navigated to another destination,
     * false otherwise
     */
    protected open fun popBackStack(): Boolean {
        if (mNavController == null) {
            mNavController = createNavController()
        }
        try {
            return mNavController?.popBackStack()!!
        } catch (e: java.lang.Exception) {
            LogUtils.w(TAG, "Navigate error:" + e.message)
            onNavigateFailed(mNavController)
        }
        return false
    }

    /**
     * Attempts to pop the controller's back stack back to a specific destination.
     *
     * @param destinationId The topmost destination to retain
     * @param inclusive     Whether the given destination should also be popped.
     * @return true if the stack was popped at least once and the user has been navigated
     * to another destination, false otherwise
     */
    protected open fun popBackStack(destinationId: Int, inclusive: Boolean): Boolean {
        if (mNavController == null) {
            mNavController = createNavController()
        }
        try {
            return mNavController?.popBackStack(destinationId, inclusive)!!
        } catch (e: java.lang.Exception) {
            LogUtils.w(TAG, "Navigate error:" + e.message)
            onNavigateFailed(mNavController)
        }
        return false
    }

    protected open fun onNavigateFailed(navController: NavController?) {}

    private fun registerInternalObserver() {
        if (!shareParentData()) {
            mViewModel.getLoadingEvent().observe(viewLifecycleOwner) { show: Boolean ->
                showLoadingView(show)
            }
            mViewModel.getToastEvent().observe(viewLifecycleOwner){toasts->
                toasts.content?.let {
                    showToast(it)
                    return@observe
                }
                toasts.resId?.let {
                    showToast(it)
                }
            }
            lifecycle.addObserver((mViewModel as LifecycleEventObserver))

        }
    }

    override fun onDestroy() {
        lifecycle.removeObserver((mViewModel as LifecycleObserver))
        super.onDestroy()
    }
}