package com.mvvm.base.lib.fragment

import android.view.View
import androidx.databinding.ViewDataBinding
import com.mvvm.base.lib.viewmodel.BaseStateViewModel
import com.mvvm.base.lib.widget.MultipleStatusView

abstract class BaseStateFragment<D : ViewDataBinding, V : BaseStateViewModel> :
    BaseFragment<D, V>() {

    override fun onBindStateView(mBinding: D): View? {
        val rootView = MultipleStatusView(requireContext())
        rootView.setOnRetryClickListener{
            onBindComplete()
        }
        rootView.addView(mBinding.root)
        return rootView
    }

    override fun onBindComplete() {
        super.onBindComplete()
        mViewModel.initMultipleStatusView(rootView as MultipleStatusView)
    }

}