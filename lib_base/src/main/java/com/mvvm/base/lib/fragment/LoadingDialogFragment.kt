package com.mvvm.base.lib.fragment

import android.view.Gravity
import com.mvvm.base.lib.R
import com.mvvm.base.lib.databinding.DialogLoadingBinding
import com.mvvm.base.lib.util.ScreenUtils

class LoadingDialogFragment : BaseDialogFragment<DialogLoadingBinding>() {

    private val WIDTH_DP = 1000F

    override fun setLayoutId(): Int = R.layout.dialog_loading

    override fun getDialogTheme(): Int = R.style.Theme_TransparentDialog

    override fun onBindComplete() {
        super.onBindComplete()
//        setWidth(ScreenUtils.dp2px(requireContext(), WIDTH_DP))
        setGravity(Gravity.CENTER or Gravity.CENTER_HORIZONTAL)
    }

}