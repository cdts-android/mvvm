/*
 * Copyright (C) GM Global Technology Operations LLC 2022.
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.mvvm.base.lib.listener;

import android.view.View;

public interface ClickHandler {

    void onClick(View view, Object... args);

    void onLongClick(View view, Object... args);
}
