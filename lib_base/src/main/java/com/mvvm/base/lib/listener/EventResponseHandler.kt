package com.mvvm.base.lib.listener

import android.os.Bundle
import android.view.View

interface EventResponseHandler {
    fun onClickResponse(view: View, bundle: Bundle?)
    fun onEventResponse(event: Int, bundle: Bundle?)
}