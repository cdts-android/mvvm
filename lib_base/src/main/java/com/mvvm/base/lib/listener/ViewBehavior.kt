package com.mvvm.base.lib.listener

interface ViewBehavior {

    fun showLoadingView(showView: Boolean)

    fun showToast(content: String)

    fun showToast(resId: Int)

}