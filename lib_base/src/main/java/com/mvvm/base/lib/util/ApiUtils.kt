package com.mvvm.base.lib.util

import com.gm.repository.net.base.ApiCode
import com.mvvm.base.lib.R

object ApiUtils {

    fun getErrMessage(code: Int): Int {
        return when (code) {
            ApiCode.CODE_NET_ERROR -> {
                R.string.status_net_err
            }
            ApiCode.CODE_NET_TIMEOUT -> {
                R.string.status_net_timeout
            }
            ApiCode.CODE_SEVER_ERROR -> {
                R.string.status_server_err
            }
            else -> {
                R.string.status_no_unkown
            }
        }
    }
}