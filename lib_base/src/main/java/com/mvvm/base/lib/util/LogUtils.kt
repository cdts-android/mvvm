package com.mvvm.base.lib.util

import android.util.Log
import com.mvvm.base.lib.util.LogUtils

object LogUtils {

    private const val TAG_FLAG = "Base_Lib."
    fun v(tag: String, msg: String?) {
        Log.v(TAG_FLAG + tag, msg!!)
    }

    fun d(tag: String, msg: String?) {
        Log.d(TAG_FLAG + tag, msg!!)
    }

    fun i(tag: String, msg: String?) {
        Log.i(TAG_FLAG + tag, msg!!)
    }

    fun w(tag: String, msg: String?) {
        Log.w(TAG_FLAG + tag, msg!!)
    }

    fun e(tag: String, msg: String?) {
        Log.e(TAG_FLAG + tag, msg!!)
    }

    fun getTraceString(tr: Throwable?): String {
        return Log.getStackTraceString(tr)
    }
}