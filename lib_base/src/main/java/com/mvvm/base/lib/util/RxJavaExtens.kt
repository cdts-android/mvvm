package com.mvvm.base.lib.util

import com.gm.repository.net.base.EmptyException
import com.mvvm.base.lib.viewmodel.BaseStateViewModel
import com.mvvm.base.lib.viewmodel.PageStateType
import com.mvvm.base.lib.widget.MultipleStatusView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.net.ConnectException
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit

fun <T> Observable<T>.async(withDelay: Long = 0): Observable<T> =
    this.subscribeOn(Schedulers.io())
        .delay(withDelay, TimeUnit.MILLISECONDS)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())


fun <T> Observable<T>.bindStatus(
    viewModel: BaseStateViewModel,
    multipleStatusView: MultipleStatusView
): Observable<T> {
        return viewModel.run {
            this@bindStatus
                .doOnSubscribe {
                    if (pageState.value != PageStateType.CONTENT) {
                        pageState.postValue(PageStateType.LOADING)
                        multipleStatusView.showLoading()
                    }

                }
                .doOnNext {
                    if (pageState.value != PageStateType.CONTENT) {
                        pageState.postValue(PageStateType.CONTENT)
                        multipleStatusView.showContent()
                    }
                }
                .doOnError {
                    if (pageState.value != PageStateType.CONTENT) {
                        if (it is EmptyException) {
                            pageState.postValue(PageStateType.EMPTY)
                            multipleStatusView.showEmpty()
                        } else if (it is UnknownHostException || it is ConnectException) {
                            multipleStatusView.showNoNetwork()
                            pageState.postValue(PageStateType.NOWORK)
                        } else {
                            multipleStatusView.showError()
                            pageState.postValue(PageStateType.ERROR)
                        }
                    }
                }
    }
}

