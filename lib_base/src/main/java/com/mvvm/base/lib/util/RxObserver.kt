package com.mvvm.base.lib.util

import com.gm.repository.net.base.ApiCode
import com.gm.repository.net.base.ApiException
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class RxObserver<T>(
    val onNext: ((T) -> Unit)? = null,
    val onError: ((Throwable) -> Unit)? = null,
    val onSubscribe: ((Disposable) -> Unit)? = null,
    val onComplete: (() -> Unit)? = null
) : Observer<T> {
    override fun onSubscribe(d: Disposable) {
        onSubscribe?.invoke(d)
    }

    override fun onNext(t: T) {
        onNext?.invoke(t)
    }

    override fun onError(e: Throwable) {
        val newThrowable = ApiException(ApiCode.SUCCESS)
        newThrowable.code = if (e is SocketTimeoutException) {
            ApiCode.CODE_NET_TIMEOUT
        } else if (e is UnknownHostException || e is ConnectException) {
            ApiCode.CODE_NET_ERROR
        } else if (e is ApiException) {
            e.code
        } else {
            ApiCode.CODE_UNKNOWN_ERROR
        }
        onError?.invoke(newThrowable)
    }

    override fun onComplete() {
        onComplete?.invoke()
    }
}