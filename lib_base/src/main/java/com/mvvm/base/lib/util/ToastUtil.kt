package com.mvvm.base.lib.util

import android.content.Context
import android.widget.Toast

object ToastUtil {
    private var mToast: Toast? = null

    fun toastShortMessage(message: String, context: Context) {
        if (mToast != null) {
            mToast!!.cancel()
            mToast = null
        }
        mToast = Toast.makeText(context, message, Toast.LENGTH_LONG)
        mToast?.show()
    }

    fun toastLongMessage(message: String, context: Context) {
        if (mToast != null) {
            mToast!!.cancel()
            mToast = null
        }
        mToast = Toast.makeText(context, message, Toast.LENGTH_LONG)
        mToast?.show()
    }

    fun toastShortMessage(resId: Int, context: Context) {
        if (mToast != null) {
            mToast!!.cancel()
            mToast = null
        }
        mToast = Toast.makeText(context, context.resources.getString(resId), Toast.LENGTH_SHORT)
        mToast?.show()
    }

    fun toastLongMessage(resId: Int, context: Context) {
        if (mToast != null) {
            mToast!!.cancel()
            mToast = null
        }
        mToast = Toast.makeText(context, context.resources.getString(resId), Toast.LENGTH_LONG)
        mToast?.show()
    }
}