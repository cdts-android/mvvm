package com.mvvm.base.lib.viewmodel

import androidx.lifecycle.*

open class BaseLifecycleViewModel : ViewModel(), LifecycleEventObserver {

    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        when (event) {
            Lifecycle.Event.ON_START -> {
                onViewVisible()
            }
            Lifecycle.Event.ON_STOP -> {
                onViewHidden()
            }
        }
    }

    open fun onViewVisible() {
        // do nothing
    }

    open fun onViewHidden() {
        // do nothing
    }

}