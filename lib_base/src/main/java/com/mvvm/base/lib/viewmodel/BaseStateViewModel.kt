package com.mvvm.base.lib.viewmodel

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import com.mvvm.base.lib.widget.MultipleStatusView


abstract class BaseStateViewModel : BaseViewModel() {

    @SuppressLint("StaticFieldLeak")
    private var mMultipleStatusView: MultipleStatusView? = null

    @PageStateType
    val pageState = MutableLiveData<Int>()

    init {
        pageState.value = (PageStateType.NORMAL)
    }

    fun initMultipleStatusView(multipleStatusView: MultipleStatusView) {
        mMultipleStatusView = multipleStatusView
    }

    protected fun getMultipleStatusView():MultipleStatusView{
        return mMultipleStatusView!!
    }
}