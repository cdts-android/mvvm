package com.mvvm.base.lib.viewmodel

import android.os.Bundle
import android.os.SystemClock
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.mvvm.base.lib.data.Toasts
import com.mvvm.base.lib.listener.ClickHandler
import com.mvvm.base.lib.listener.EventResponseHandler
import com.mvvm.base.lib.listener.ViewBehavior
import com.mvvm.base.lib.util.LogUtils
import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel : BaseLifecycleViewModel(), ViewBehavior, ClickHandler {

    companion object {
        const val MINIMUM_INTERVAL = 700L
        const val TAG = "BaseViewModel"
    }

    open val mCompositeDisposable: CompositeDisposable by lazy {
        CompositeDisposable()
    }
    private var mLastClickTime = 0L
    private val mLoadingEvent = MutableLiveData<Boolean>()
    private val mToastEvent: MutableLiveData<Toasts> = MutableLiveData<Toasts>()
    private var mEventResponseHandlers: HashSet<EventResponseHandler>? = null

    fun getLoadingEvent(): MutableLiveData<Boolean> = mLoadingEvent
    fun getToastEvent(): MutableLiveData<Toasts> = mToastEvent

    fun registerEventListener(eventResponseHandler: EventResponseHandler?) {
        eventResponseHandler?.let {
            if (mEventResponseHandlers == null) {
                mEventResponseHandlers = HashSet()
            }
            mEventResponseHandlers?.add(eventResponseHandler)
        }
    }

    fun unregisterEventListener(eventResponseHandler: EventResponseHandler?) {
        eventResponseHandler?.let {
            mEventResponseHandlers?.let {
                mEventResponseHandlers?.remove(eventResponseHandler)
            }
        }
    }

    fun notifyEventResponseHandlers(eventId: Int, bundle: Bundle?) {
        mEventResponseHandlers?.let {
            it.map { eventResponseHandler ->
                eventResponseHandler.onEventResponse(eventId, bundle)
            }
        }
    }


    fun notifyClickResponseHandlers(view: View, bundle: Bundle?) {
        mEventResponseHandlers?.let {
            it.map { eventResponseHandler ->
                eventResponseHandler.onClickResponse(view, bundle)
            }
        }
    }

    override fun showLoadingView(showView: Boolean) {
        mLoadingEvent.postValue(showView)
    }

    override fun showToast(content: String) {
        mToastEvent.postValue(Toasts(content = content))
    }

    override fun showToast(resId: Int) {
        mToastEvent.postValue(Toasts(resId = resId))
    }

    override fun onClick(view: View, vararg args: Any?) {
        if (SystemClock.elapsedRealtime() - mLastClickTime < MINIMUM_INTERVAL) {
            mLastClickTime = SystemClock.elapsedRealtime()
            LogUtils.d(TAG, "Avoid quick clicks, ignore this event.")
            return
        }
        handleClickEvent(view, args)
    }

    override fun onLongClick(view: View, vararg args: Any?) {
        handleOnLongClick(view, args)
    }

    open fun handleClickEvent(view: View, vararg args: Any?) {}

    open fun handleOnLongClick(view: View, vararg args: Any?) {}

    override fun onCleared() {
        mCompositeDisposable.clear()
        super.onCleared()
    }

}