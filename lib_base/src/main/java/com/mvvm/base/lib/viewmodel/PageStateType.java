package com.mvvm.base.lib.viewmodel;

import androidx.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;



@IntDef({PageStateType.LOADING, PageStateType.EMPTY, PageStateType.ERROR, PageStateType.NOWORK, PageStateType.NORMAL, PageStateType.CONTENT})
@Retention(RetentionPolicy.SOURCE)
public @interface PageStateType {

    int NOWORK = -4;

    int LOADING = -3;

    int EMPTY = -2;

    int ERROR = -1;

    int NORMAL = 0;

    int CONTENT = 1;
}
