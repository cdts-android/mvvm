package com.mvvm.base.lib.widget

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.mvvm.base.lib.R
import java.util.*


class MultipleStatusView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : FrameLayout(context, attrs, defStyleAttr) {

    companion object {
        private val DEFAULT_LAYOUT_PARAMS = LayoutParams(LayoutParams.MATCH_PARENT,
            LayoutParams.MATCH_PARENT)

        const val STATUS_CONTENT = 0x00
        const val STATUS_LOADING = 0x01
        const val STATUS_EMPTY = 0x02
        const val STATUS_ERROR = 0x03
        const val STATUS_NO_NETWORK = 0x04

        private const val NULL_RESOURCE_ID = -1
    }

    private var mEmptyView: View? = null
    private var mErrorView: View? = null
    private var mLoadingView: View? = null
    private var mNoNetworkView: View? = null
    private var mContentView: View? = null
    private val mEmptyViewResId: Int
    private val mErrorViewResId: Int
    private val mLoadingViewResId: Int
    private val mNoNetworkViewResId: Int
    private val mContentViewResId: Int

    var viewStatus: Int = 0
    private val mInflater: LayoutInflater
    private var mOnRetryClickListener: OnClickListener? = null
    private val mOtherIds = ArrayList<Int>()


    init {
        @SuppressLint("CustomViewStyleable") val a = context.obtainStyledAttributes(attrs, R.styleable.MultipleStatusView, defStyleAttr, 0)
        mEmptyViewResId = a.getResourceId(R.styleable.MultipleStatusView_emptyView, R.layout.public_status_empty_view)
        mErrorViewResId = a.getResourceId(R.styleable.MultipleStatusView_errorView, R.layout.public_status_error_view)
        mLoadingViewResId = a.getResourceId(R.styleable.MultipleStatusView_loadingView, R.layout.public_status_loading_view)
        mNoNetworkViewResId = a.getResourceId(R.styleable.MultipleStatusView_noNetworkView, R.layout.public_status_no_network_view)
        mContentViewResId = a.getResourceId(R.styleable.MultipleStatusView_contentView, NULL_RESOURCE_ID)
        a.recycle()
        mInflater = LayoutInflater.from(getContext())
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        showContent()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        clear(mEmptyView, mLoadingView, mEmptyView, mNoNetworkView)
        mOtherIds.clear()
        if (mOnRetryClickListener != null) {
            mOnRetryClickListener = null
        }
    }

    fun setOnRetryClickListener(onRetryClickListener: View.OnClickListener) {
        this.mOnRetryClickListener = onRetryClickListener
    }

    @JvmOverloads
    fun showEmpty(layoutId: Int = mEmptyViewResId, layoutParams: ViewGroup.LayoutParams = DEFAULT_LAYOUT_PARAMS) {
        showEmpty(mEmptyView ?: inflateView(layoutId), layoutParams)
    }

    private fun showEmpty(view: View, layoutParams: ViewGroup.LayoutParams) {
        viewStatus = STATUS_EMPTY
        if (mEmptyView == null) {
            mEmptyView = view
            val emptyRetryView = mEmptyView!!.findViewById<View>(R.id.empty_view)
            if (mOnRetryClickListener != null && emptyRetryView != null) {
                emptyRetryView.setOnClickListener(mOnRetryClickListener)
            }
            mOtherIds.add(mEmptyView!!.id)
            addView(mEmptyView, 0, layoutParams)
        }else {
            if(mEmptyView?.parent == null){
                if(!mOtherIds.contains(mEmptyView!!.id)){
                    mOtherIds.add(mEmptyView!!.id)
                }
                addView(mEmptyView, 0, layoutParams)
            }
        }
        showViewById(mEmptyView!!.id)
    }

    @JvmOverloads
    fun showError(layoutId: Int = mErrorViewResId, layoutParams: ViewGroup.LayoutParams = DEFAULT_LAYOUT_PARAMS) {
        showError(mErrorView ?: inflateView(layoutId), layoutParams)
    }

    private fun showError(view: View, layoutParams: ViewGroup.LayoutParams) {
        viewStatus = STATUS_ERROR
        if (mErrorView == null) {
            mErrorView = view
            if (mOnRetryClickListener != null && mErrorView != null) {
                mErrorView!!.setOnClickListener(mOnRetryClickListener)
            }
            mOtherIds.add(mErrorView!!.id)
            addView(mErrorView, 0, layoutParams)
        }else {
            if(mErrorView?.parent == null){
                if(!mOtherIds.contains(mErrorView!!.id)){
                    mOtherIds.add(mErrorView!!.id)
                }
                addView(mErrorView, 0, layoutParams)
            }
        }
        showViewById(mErrorView!!.id)
    }

    @JvmOverloads
    fun showLoading(layoutId: Int = mLoadingViewResId, layoutParams: ViewGroup.LayoutParams = DEFAULT_LAYOUT_PARAMS) {
        showLoading(mLoadingView ?: inflateView(layoutId), layoutParams)
    }

    private fun showLoading(view: View, layoutParams: ViewGroup.LayoutParams) {
        viewStatus = STATUS_LOADING
        if (mLoadingView == null) {
            mLoadingView = view
            mOtherIds.add(mLoadingView!!.id)
            addView(mLoadingView, 0, layoutParams)
        }
        showViewById(mLoadingView!!.id)
    }

    @JvmOverloads
    fun showNoNetwork(layoutId: Int = mNoNetworkViewResId, layoutParams: ViewGroup.LayoutParams = DEFAULT_LAYOUT_PARAMS) {
        showNoNetwork(mNoNetworkView ?: inflateView(layoutId), layoutParams)
    }

    private fun showNoNetwork(view: View, layoutParams: ViewGroup.LayoutParams) {
        viewStatus = STATUS_NO_NETWORK
        if (mNoNetworkView == null) {
            mNoNetworkView = view
            val noNetworkRetryView = mNoNetworkView!!.findViewById<View>(R.id.no_network_view)
            if (mOnRetryClickListener != null && noNetworkRetryView != null) {
                noNetworkRetryView.setOnClickListener(mOnRetryClickListener)
            }
            mOtherIds.add(mNoNetworkView!!.id)
            addView(mNoNetworkView, 0, layoutParams)
        }
        showViewById(mNoNetworkView!!.id)
    }

    fun showContent() {
        viewStatus = STATUS_CONTENT
        if (mContentView == null && mContentViewResId != NULL_RESOURCE_ID) {
            mContentView = mInflater.inflate(mContentViewResId, null)
            addView(mContentView, 0, DEFAULT_LAYOUT_PARAMS)
        }
        showContentView()
    }

    private fun inflateView(layoutId: Int): View {
        return mInflater.inflate(layoutId, null)
    }

    private fun showViewById(viewId: Int) {
        val childCount = childCount
        for (i in 0 until childCount) {
            val view = getChildAt(i)
            view.visibility = if (view.id == viewId) View.VISIBLE else View.GONE
        }
    }

    private fun showContentView() {
        val childCount = childCount
        for (i in 0 until childCount) {
            val view = getChildAt(i)
            view.visibility = if (mOtherIds.contains(view.id)) View.GONE else View.VISIBLE
        }
    }


    private fun clear(vararg views: View?) {
        for (view in views) {
            view?.let {
                this.removeView(it)
            }
        }
    }
}