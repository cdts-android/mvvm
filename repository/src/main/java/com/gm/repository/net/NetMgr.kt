package com.gm.repository.net

import android.annotation.SuppressLint
import android.util.Log
import com.gm.repository.net.base.*
import com.gm.repository.net.util.HttpsUtils
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory

import retrofit2.converter.gson.GsonConverterFactory
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.net.ssl.X509TrustManager
import kotlin.jvm.Throws


/***
 * Network request encapsulation class
 * Retrofit+OkHttp
 */

object NetMgr {
    //A map that stores network settings
    private val providerMap = HashMap<String, NetProvider>()

    //A map that stores retrofit
    private val retrofitMap = HashMap<String, Retrofit>()

    //A map that stores okHttpClient
    private val clientMap = HashMap<String, OkHttpClient>()

    private const val connectTimeoutMills = 10 * 1000L
    private const val readTimeoutMills = 10 * 1000L

    var commonProvider: NetProvider? = null
        private set

    /**
     * get the retrofit instance
     */
    @JvmOverloads
    fun getRetrofit(
        baseUrl: String? = RepositoryUrlConstants.getBaseUrl(),
        netProvider: NetProvider? = BaseNetProvider()
    ): Retrofit {
        var provider = netProvider
        if (empty(baseUrl)) {
            throw IllegalStateException("baseUrl can not be null")
        }
        if (retrofitMap[baseUrl] != null) {
            return retrofitMap[baseUrl]!!
        }

        if (provider == null) {
            provider = providerMap[baseUrl]
            if (provider == null) {
                provider = commonProvider
            }
        }

        checkProvider(provider)

        val gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
            .registerTypeAdapterFactory(NullStringToEmptyAdapterFactory())
            .create()

        val builder = Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(getClient(baseUrl!!, provider!!))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        builder.addConverterFactory(GsonConverterFactory.create(gson))
        val retrofit = builder.build()
        retrofitMap[baseUrl] = retrofit
        providerMap[baseUrl] = provider
        return retrofit
    }

    private fun getClient(baseUrl: String, provider: NetProvider): OkHttpClient {
        if (empty(baseUrl)) {
            throw IllegalStateException("baseUrl can not be null")
        }

        if (clientMap[baseUrl] != null) {
            return clientMap[baseUrl]!!
        }

        checkProvider(provider)

        val builder = OkHttpClient.Builder()

        builder.connectTimeout(
            if (provider.configConnectTimeoutSecs() != 0L)
                provider.configConnectTimeoutSecs()
            else
                connectTimeoutMills, TimeUnit.SECONDS
        )

        builder.readTimeout(
            if (provider.configReadTimeoutSecs() != 0L)
                provider.configReadTimeoutSecs()
            else
                readTimeoutMills, TimeUnit.SECONDS
        )

        builder.writeTimeout(
            if (provider.configWriteTimeoutSecs() != 0L)
                provider.configWriteTimeoutSecs()
            else
                readTimeoutMills, TimeUnit.SECONDS
        )


        provider.configHttps(builder)


        val handler = provider.configHandler()
        builder.addInterceptor(NetInterceptor(handler))

        val interceptors = provider.configInterceptors()
        if (!empty(interceptors)) {
            interceptors!!.forEach {
                builder.addInterceptor(it)
            }
        }
        //Add debug interceptor
        if (provider.configLogEnable()) {
            val loggingInterceptor = HttpLoggingInterceptor {
                Log.i("OkHttp", it)
            }
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(loggingInterceptor)
        }

        //Customize trust rules to verify server certificates
        val sslParams = HttpsUtils.getSslSocketFactory(@SuppressLint("CustomX509TrustManager")
        object : X509TrustManager {
            @SuppressLint("TrustAllX509TrustManager")
            @Throws(CertificateException::class)
            override fun checkClientTrusted(
                x509Certificates: Array<X509Certificate>,
                s: String
            ) {
            }

            @SuppressLint("TrustAllX509TrustManager")
            @Throws(CertificateException::class)
            override fun checkServerTrusted(
                x509Certificates: Array<X509Certificate>?,
                s: String
            ) {
                return
            }

            override fun getAcceptedIssuers(): Array<X509Certificate?> {
                return arrayOfNulls(0)
            }
        })
        builder.sslSocketFactory(sslParams.sSLSocketFactory, sslParams.trustManager)

        val client = builder.build()
        clientMap[baseUrl] = client
        providerMap[baseUrl] = provider
        return client
    }


    /**
     * Determine whether baseUrl is empty
     */
    private fun empty(baseUrl: String?): Boolean {
        return baseUrl == null || baseUrl.isEmpty()
    }

    /**
     * Determine whether the interceptor is empty
     */
    private fun empty(interceptors: Array<Interceptor>?): Boolean {
        return interceptors == null || interceptors.isEmpty()
    }

    /**
     * Determine whether there is an interceptor
     */
    private fun checkProvider(provider: NetProvider?) {
        if (provider == null) {
            throw IllegalStateException("must register provider first")
        }
    }

    /**
     * Get the map collection of retrofit instances
     */
    fun getRetrofitMap(): Map<String, Retrofit> {
        return retrofitMap
    }

    /**
     * Get the map collection of okHttpClient instances
     */
    fun getClientMap(): Map<String, OkHttpClient> {
        return clientMap
    }

    /**
     * Get the interface instance of the specific network request Service
     */
    operator fun <S> get(baseUrl: String, service: Class<S>): S {
        return getRetrofit(baseUrl).create(service)
    }

    operator fun <S> get(service: Class<S>): S {
        return getRetrofit(RepositoryUrlConstants.getBaseUrl()).create(service)
    }

    /**
     * Register network request interceptor
     */
    fun registerProvider(provider: NetProvider) {
        commonProvider = provider
    }

    /**
     * Register network request interceptor
     */
    fun registerProvider(baseUrl: String, provider: NetProvider) {
        providerMap[baseUrl] = provider
    }

    /**
     * Clear the map collection
     */
    fun clearCache() {
        retrofitMap.clear()
        clientMap.clear()
    }
}