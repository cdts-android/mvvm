package com.gm.repository.net.base

object ApiCode {
    // default code
    const val SUCCESS = 0

    const val CODE_NET_ERROR=10001
    const val CODE_NET_TIMEOUT=10002
    const val CODE_UNKNOWN_ERROR=10003
    const val CODE_REQUEST_PRARM_ID=10004
    const val CODE_SEVER_ERROR=10005
    const val CODE_NO_DATA=10006



}