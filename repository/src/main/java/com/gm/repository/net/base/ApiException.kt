package com.gm.repository.net.base

import java.io.IOException

/***
 * Network request error exception class
 */

open class ApiException(var code: Int,desc: String?="") : IOException(desc)
