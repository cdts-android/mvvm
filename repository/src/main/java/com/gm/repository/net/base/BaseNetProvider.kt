package com.gm.repository.net.base

import com.gm.repository.BuildConfig
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

import okhttp3.*
import java.nio.charset.Charset


open class BaseNetProvider : NetProvider {

    companion object {
        const val CONNECT_TIME_OUT: Long = 10
        const val READ_TIME_OUT: Long = 10
        const val WRITE_TIME_OUT: Long = 30
    }

    private val normalCode = arrayOf(ApiCode.SUCCESS)

    override fun configInterceptors(): Array<Interceptor>? = null

    override fun configHttps(builder: OkHttpClient.Builder) {}

    override fun configHandler(): RequestHandler = HeaderHandler()

    override fun configConnectTimeoutSecs(): Long =
            CONNECT_TIME_OUT

    override fun configReadTimeoutSecs(): Long =
            READ_TIME_OUT

    override fun configWriteTimeoutSecs(): Long =
            WRITE_TIME_OUT

    override fun configLogEnable(): Boolean = !(BuildConfig.DEBUG)

    inner class HeaderHandler : RequestHandler {
        override fun onBeforeRequest(request: Request, chain: Interceptor.Chain): Request {
            // TODO: Add and handle request headers
            return requestBuild(request)

        }

        private fun requestBuild(request: Request): Request {
            val req = request.newBuilder()
            return req.build()

        }

        override fun onAfterRequest(response: Response, chain: Interceptor.Chain): Response {

            if (response.isSuccessful) {
                val source = response.body()?.source()
                source?.request(Long.MAX_VALUE)
                val buffer = source?.buffer()
                val body = buffer?.clone()?.readString(Charset.forName("UTF-8"))

                val bean: BaseData<Any>? = Gson().fromJson(body, object : TypeToken<BaseData<Any>>() {}.type)
                return if (bean != null){
                    return response
                }else{
                    throw ApiException(ApiCode.CODE_SEVER_ERROR)
                }
            } else {
                throw ApiException(ApiCode.CODE_SEVER_ERROR)
            }
        }
    }
}