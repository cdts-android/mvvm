package com.gm.repository.net.base

import java.io.IOException

class EmptyException() : IOException()