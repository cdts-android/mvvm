package com.gm.repository.net.base

import okhttp3.Interceptor
import okhttp3.OkHttpClient

interface NetProvider {
    /**
     * Add network request interceptor
     */
    fun configInterceptors(): Array<Interceptor>?

    /**
     * Add https request interceptor
     */
    fun configHttps(builder: OkHttpClient.Builder)


    /**
     * add interceptor
     */
    fun configHandler(): RequestHandler

    /**
     * link timeout
     */
    fun configConnectTimeoutSecs(): Long

    /**
     * read timeout
     */
    fun configReadTimeoutSecs(): Long

    /**
     * write timeout
     */
    fun configWriteTimeoutSecs(): Long

    /**
     * Whether to enable log printing log
     */
    fun configLogEnable(): Boolean
}
