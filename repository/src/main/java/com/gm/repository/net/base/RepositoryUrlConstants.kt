package com.gm.repository.net.base

import com.gm.repository.BuildConfig


object RepositoryUrlConstants {

    private const val DEV_BASE_URL = "https://www.baidu.com"

    private const val RELEASE_BASE_URL = "https://www.baidu.com"

    fun getBaseUrl(): String = if (BuildConfig.DEBUG) DEV_BASE_URL else RELEASE_BASE_URL

}