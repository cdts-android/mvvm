package com.gm.repository.net.base

import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import java.io.IOException

/***
 * network request interceptor
 */

interface RequestHandler {
    /**
     * Initiate request interception method
     */
    fun onBeforeRequest(request: Request, chain: Interceptor.Chain): Request

    /**
     * Initiate response interception method
     */
    @Throws(IOException::class)
    fun onAfterRequest(response: Response, chain: Interceptor.Chain): Response
}