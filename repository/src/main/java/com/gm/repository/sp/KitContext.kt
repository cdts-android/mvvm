package com.gm.repository.sp

import android.content.Context

class KitContext {
    lateinit var context: Context
        private set

    fun init(context: Context) {
        this.context = context
    }

    fun getSp(name: String = GlobalSp.file_name) = context.getSharedPreferences(name, Context.MODE_PRIVATE)

    companion object {
        val instance: KitContext by lazy(mode = LazyThreadSafetyMode.SYNCHRONIZED) {
            KitContext()
        }
    }

}