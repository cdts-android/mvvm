package com.gm.repository.sp

import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

fun <T> proxy(initializer: () -> T): Proxy<T> = SynchronizedLazyImpl(initializer)

class SynchronizedLazyImpl<out T>(val initializer: () -> T, lock: Any? = null): Proxy<T> {
    private val lock = lock ?: this

    override val value: T
        get() {
            return synchronized(lock) {
                initializer()
            }
        }
}

interface Proxy<out T> : ReadOnlyProperty<Any?, T> {

    open val value: T

    override fun getValue(thisRef: Any?, property: KProperty<*>): T = value

}

