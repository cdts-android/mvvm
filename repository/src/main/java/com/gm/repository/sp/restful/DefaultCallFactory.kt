package com.gm.repository.sp.restful

import java.util.concurrent.LinkedBlockingQueue

class DefaultCallFactory: CallFactory {
    override fun <Resp> create() = DefaultCallAdapter<Resp>()
}

class DefaultCallAdapter<Resp>: CallAdapter<Resp> {
    override fun apply(request: Request) =
            DefaultCall<Resp>(DefaultRequest(request))

}

class DefaultCall<Resp>(private val request: DefaultRequest):
        Call<Resp> {
    @Volatile
    private var canceled: Boolean = false
    @Volatile
    private var executed: Boolean = false

    override fun execute(): Resp {
        val queue = LinkedBlockingQueue<Any>(1)
        enqueue(object : Callback<Resp> {
            override fun callFail(msg: Throwable) {
                queue.put(msg)
            }

            override fun callSuccess(any: Resp) {
                queue.put(any)
            }

        })

        val result = queue.take()
        return if (result is Throwable) {
            throw result
        } else {
            result as Resp
        }
    }

    override fun enqueue(callback: Callback<Resp>) {
        synchronized(this) {
            if (executed) throw IllegalStateException("Already executed.")
            executed = true
        }
    }

    override fun isExecuted() = executed

    override fun isCancel() = canceled

    override fun cancel() {
        canceled = true
    }

    override fun clone() = DefaultCall<Resp>(request)
}

class DefaultRequest(val request: Request) {
}