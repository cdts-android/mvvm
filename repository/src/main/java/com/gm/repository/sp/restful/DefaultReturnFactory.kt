package com.gm.repository.sp.restful

import io.reactivex.*
import io.reactivex.disposables.Disposable
import io.reactivex.exceptions.CompositeException
import io.reactivex.exceptions.Exceptions
import io.reactivex.plugins.RxJavaPlugins

class DefaultReturnFactory: ReturnFactory {
    override fun <Resp, Return> create() = DefaultReturnAdapter<Resp, Return>()
}

class DefaultReturnAdapter<Resp, Return>: ReturnAdapter<Resp, Return> {
    override fun apply(call: Call<Resp>, resultType: Class<*>): Return {
        return when(resultType) {
            Observable::class.java -> DefaultEnqueueObservable(call)
            Flowable::class.java -> DefaultEnqueueObservable(call).toFlowable(BackpressureStrategy.LATEST)
            Single::class.java -> DefaultEnqueueObservable(call).singleOrError()
            Maybe::class.java -> DefaultEnqueueObservable(call).singleElement()
            Call::class.java -> call
            else -> call.execute()
        } as Return
    }

}

private class DefaultEnqueueObservable<T>(private val originalCall: Call<T>) : Observable<T>() {

    override fun subscribeActual(subscriber: Observer<in T>) {
        val call = originalCall.clone()
        val callback = CallCallback(call, subscriber)
        subscriber.onSubscribe(callback)
        if (!callback.isDisposed) {
            call.enqueue(callback)
        }
    }

    private class CallCallback<T> internal constructor(
            private val call: Call<*>,
            private val observer: Observer<in T>
    ) : Disposable, Callback<T> {

        @Volatile
        private var disposed: Boolean = false
        internal var terminated = false

        override fun callSuccess(response: T) {
            if (disposed) return

            try {
                observer.onNext(response)

                if (!disposed) {
                    terminated = true
                    observer.onComplete()
                }

            } catch (t: Throwable) {
                if (terminated) {
                    RxJavaPlugins.onError(t)
                } else if (!disposed) {
                    try {
                        observer.onError(t)
                    } catch (inner: Throwable) {
                        Exceptions.throwIfFatal(inner)
                        RxJavaPlugins.onError(CompositeException(t, inner))
                    }

                }
            }
        }

        override fun callFail(t: Throwable) {
            if (disposed) return

            try {
                observer.onError(t)
            } catch (inner: Throwable) {
                Exceptions.throwIfFatal(inner)
                RxJavaPlugins.onError(CompositeException(t, inner))
            }
        }

        override fun dispose() {
            disposed = true
            call.cancel()
        }

        override fun isDisposed(): Boolean {
            return disposed
        }
    }
}
