package com.gm.repository.sp.restful

import com.gm.repository.sp.sp.SpCallFactory
import com.gm.repository.sp.sp.SpReturnFactory
import java.io.ByteArrayOutputStream
import java.io.PrintStream
import java.lang.Exception

internal fun safe(block: () -> Unit): Boolean {
    return try {
        block()
        true
    } catch (e : Exception) {
        ByteArrayOutputStream().use {
            e.printStackTrace(PrintStream(it))
        }
        false
    }
}

fun <T> tryException(default: T? = null, block: () -> T): T? {
    return try {
        block()
    } catch (e : Exception) {
        ByteArrayOutputStream().use {
            e.printStackTrace(PrintStream(it))
        }
        default
    }
}

fun Restful.Builder.injectSp() = this.setCallFactory(SpCallFactory()).setReturnFactory(SpReturnFactory())