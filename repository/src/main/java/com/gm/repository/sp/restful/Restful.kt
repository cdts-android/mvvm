package com.gm.repository.sp.restful

import java.lang.reflect.Method
import java.lang.reflect.Proxy
import java.util.concurrent.ConcurrentHashMap

class Restful(internal val callFactory: CallFactory, internal val returnFactory: ReturnFactory) {

    private val cache = ConcurrentHashMap<Method, ServiceMethod<*, *>>()

    fun <T> create(service: Class<T>): T {
        return Proxy.newProxyInstance(service.classLoader, arrayOf<Class<*>>(service)) { _, method, args ->
            loadServiceMethod(method).invoke(args ?: emptyArray())
        } as T
    }

    private fun loadServiceMethod(method: Method): ServiceMethod<*, *> {
        var result = cache[method]
        if (result != null) {
            return result
        }

        synchronized(cache) {
            result = cache[method]
            if (result == null) {
                result = ServiceMethod.create<Any,  Any>(this, method)
                cache[method] = result!!
            }
        }

        return result!!
    }

    class Builder {
        private var callFactory: CallFactory? = null
        private var returnFactory: ReturnFactory? = null

        fun setCallFactory(callFactory: CallFactory): Builder {
            this.callFactory = callFactory
            return this
        }

        fun setReturnFactory(returnFactory: ReturnFactory): Builder {
            this.returnFactory = returnFactory
            return this
        }

        fun build(): Restful {
            if (callFactory == null) {
                callFactory = DefaultCallFactory()
            }

            if (returnFactory == null) {
                returnFactory = DefaultReturnFactory()
            }

            return Restful(callFactory!!, returnFactory!!)
        }
    }

}

class ServiceMethod<Resp, Return>(
        private val factory: RequestFactory,
        private val callAdapter: CallAdapter<Resp>,
        private val returnAdapter: ReturnAdapter<Resp, Return>
) {
    fun invoke(args: Array<Any>): Return {
        val request = factory.create(args)
        val call = callAdapter.apply(request)

        val resultType = factory.getResultType()
        return returnAdapter.apply(call, resultType)
    }

    companion object {
        fun <Resp, Return> create(restful: Restful, method: Method) =
                ServiceMethod<Resp, Return>(
                        RequestFactory.create(method),
                        restful.callFactory.create(),
                        restful.returnFactory.create()
                )
    }
}

class RequestFactory(private val method: Method) {

    private var methods = method.annotations
    private var params = method.parameterAnnotations

    fun getResultType() = method.returnType

    fun create(array: Array<Any>) = Request.Builder().apply {

        putMethodAnnotation(methods)

        for (index in array.indices) {
            putParamsAnnotation(params[index], array[index])
        }

    }.build()

    companion object {
        fun create(method: Method) = RequestFactory(method)
    }
}

class Request(val methods: Array<Annotation>?, val params: Map<Array<Annotation>, Any?>) {

    class Builder {
        private var methods: Array<Annotation>? = null
        private var params = mutableMapOf<Array<Annotation>, Any?>()

        fun putMethodAnnotation(annotation: Array<Annotation>): Builder {
            this.methods = annotation
            return this
        }

        fun putParamsAnnotation(annotation: Array<Annotation>, param: Any?): Builder {
            this.params[annotation] = param
            return this
        }

        fun build() = Request(methods, params)
    }
}

interface CallFactory {
    fun <Resp> create(): CallAdapter<Resp>
}

interface CallAdapter<T> {
    fun apply(request: Request): Call<T>
}

interface ReturnFactory {
    fun <Resp, Return> create(): ReturnAdapter<Resp, Return>
}

interface ReturnAdapter<Resp, Return> {
    fun apply(any: Call<Resp>, resultType: Class<*>): Return
}

interface Call<T> {
    fun execute(): T
    fun enqueue(callback: Callback<T>)
    fun isExecuted(): Boolean
    fun isCancel(): Boolean
    fun cancel()
    fun clone(): Call<T>
}

interface Callback<T> {
    fun callSuccess(any: T)
    fun callFail(msg: Throwable)
}


