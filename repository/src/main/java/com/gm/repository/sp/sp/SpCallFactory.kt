package com.gm.repository.sp.sp

import com.gm.repository.R
import com.gm.repository.sp.KitContext
import com.gm.repository.sp.restful.*
import java.lang.reflect.Type
import java.util.concurrent.LinkedBlockingQueue

class SpCallFactory: CallFactory {
    override fun <Resp> create() = SpCallAdapter<Resp>()
}

class SpCallAdapter<Resp>: CallAdapter<Resp> {
    override fun apply(request: Request) =
        SpCall<Resp>(SpRequest(request))

}

class SpCall<Resp>(private val request: SpRequest): Call<Resp> {

    @Volatile
    private var canceled: Boolean = false
    @Volatile
    private var executed: Boolean = false

    private val type by lazy { request.type }

    val key: String by lazy { request.key }

    val proxy: SpProxy by lazy { request.proxy }


    override fun execute(): Resp {
        val queue = LinkedBlockingQueue<Any>()
        enqueue(object : Callback<Resp> {
            override fun callFail(msg: Throwable) {
                queue.put(msg)
            }

            override fun callSuccess(any: Resp) {
                queue.put(any)
            }
        })

        val result = queue.take()
        return if (result is Throwable) {
            throw result
        } else {
            result as Resp
        }
    }

    override fun enqueue(callback: Callback<Resp>) {
        synchronized(this) {
            if (executed) throw IllegalStateException("Already executed.")
            executed = true
        }

        callEnqueue(callback)
    }

    fun callEnqueue(callback: Callback<Resp>) {
        if (canceled) {
            callback.callFail(RuntimeException("任务被取消"))
        }

        with(request.cmd) {
            if (this is SpInsert) {
                callback.callSuccess(proxy.putAny(request.key, request.value) as Resp)
            }

            if (this is SpQuery) {
                callback.callSuccess(
                   proxy.getAny(request.key, default, type) as Resp
                )
            }

        }
    }

    override fun isExecuted() = executed

    override fun isCancel() = canceled

    override fun cancel() {
        canceled = true
    }

    override fun clone() = SpCall<Resp>(request)
}

class SpRequest(val request: Request) {
    val cmd: Annotation
        get() {
            if (request.methods != null) {
                for (annotation in request.methods!!) {
                    if (annotation is SpInsert) {
                        return annotation
                    }
                    if (annotation is SpQuery) {
                        return annotation
                    }
                }
            }

            throw RuntimeException(KitContext.instance.context.resources.getString(R.string.sp_no_find_sp_value_and_type))
        }

    val key: String
        get() {
            val innerPath = _path ?: return _key
            return _key.replace("@${innerPath.first}", innerPath.second.toString())
        }

    val type: Type
        get() {
            for ((key, value) in request.params) {
                for (annotation in key) {
                    if (annotation is SpType) {
                        return value as Type
                    }
                }
            }

            throw RuntimeException(KitContext.instance.context.resources.getString(R.string.sp_no_find_sp_type))
        }

    val proxy: SpProxy
        get() {
            for ((key, value) in request.params) {
                for (annotation in key) {
                    if (annotation is SpRefer) {
                        return value as SpProxy
                    }
                }
            }

            return SpProxy.getInstance()
        }
    val value: Any
        get() {
            for ((key, value) in request.params) {
                for (annotation in key) {
                    if (annotation is SpValue) {
                        return value!!
                    }
                }
            }

            throw RuntimeException(KitContext.instance.context.resources.getString(R.string.sp_no_find_sp_value))
        }

    private val _key: String
        get() {
            with(cmd) {
                if (this is SpInsert) {
                    return key
                }

                if (this is SpQuery) {
                    return key
                }

                throw RuntimeException(KitContext.instance.context.resources.getString(R.string.sp_no_find_sp_value_and_type))
            }
        }

    private val _path: Pair<String, Any?>?
        get() {
            for ((key, value) in request.params) {
                for (annotation in key) {
                    if (annotation is SpPath) {
                        return Pair(annotation.value, value)
                    }
                }
            }

            return null
        }
}