package com.gm.repository.sp.sp

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class SpInsert(val key: String)