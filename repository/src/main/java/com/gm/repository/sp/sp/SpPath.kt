package com.gm.repository.sp.sp

@Target(AnnotationTarget.VALUE_PARAMETER)
@Retention(AnnotationRetention.RUNTIME)
annotation class SpPath(val value: String)