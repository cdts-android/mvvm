package com.gm.repository.sp.sp

import android.content.Context
import android.content.SharedPreferences
import android.util.Base64
import com.gm.repository.R
import com.gm.repository.sp.GlobalSp
import com.gm.repository.sp.KitContext
import com.google.gson.*
import java.lang.reflect.Type

class SpProxy(private val sp: SharedPreferences): SharedPreferences by sp {

    fun putAny(key: String, value: Any) =
        sp.edit().putString(key, getGson().toJson(value)).commit()

    fun <T> getAny(key: String, default: String, type: Type) =
        getGson().fromJson<T>(sp.getString(key, default), type)

    private fun getGson(): Gson {
        return GsonBuilder().registerTypeAdapter(ByteArray::class.java, ByteArrayTypeAdapter()).create()
    }

    companion object {
        private var instance: SpProxy? = null
        fun create(context: Context) {
            instance = SpProxy(context.getSharedPreferences(GlobalSp.file_name, Context.MODE_PRIVATE))
        }

        fun release() {
            instance = null
        }

        fun getInstance() = checkNotNull(instance) { KitContext.instance.context.resources.getString(R.string.sp_not_init) }
    }
}

class ByteArrayTypeAdapter : JsonSerializer<ByteArray>, JsonDeserializer<ByteArray> {
    override fun serialize(src: ByteArray, typeOfSrc: Type?, context: JsonSerializationContext?): JsonElement {
        return JsonPrimitive(Base64.encodeToString(src, 0, src.size, Base64.DEFAULT))
    }

    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): ByteArray {
        if (json !is JsonPrimitive) {
            throw JsonParseException(KitContext.instance.context.resources.getString(R.string.sp_data_should_be_string))
        }
        return Base64.decode(json.asString, Base64.DEFAULT)
    }

}