package com.gm.repository.sp.sp

@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.RUNTIME)
annotation class SpQuery(val key: String, val default: String = "")