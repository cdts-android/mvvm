package com.gm.repository.sp.sp

import android.content.SharedPreferences
import com.gm.repository.sp.restful.Call
import com.gm.repository.sp.restful.Callback
import com.gm.repository.sp.restful.ReturnAdapter
import com.gm.repository.sp.restful.ReturnFactory
import io.reactivex.*
import io.reactivex.disposables.Disposable
import io.reactivex.exceptions.CompositeException
import io.reactivex.exceptions.Exceptions
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers

class SpReturnFactory: ReturnFactory {
    override fun <Resp, Return> create() = SpReturnAdapter<Resp, Return>()
}

class SpReturnAdapter<Resp, Return>: ReturnAdapter<Resp, Return> {
    override fun apply(call: Call<Resp>, resultType: Class<*>): Return {
        return when(resultType) {
            Observable::class.java -> SpEnqueueObservable(call as SpCall<Resp>)
            Flowable::class.java -> SpEnqueueObservable(call as SpCall<Resp>).toFlowable(BackpressureStrategy.LATEST)
            Single::class.java -> SpEnqueueObservable(call as SpCall<Resp>).singleOrError()
            Maybe::class.java -> SpEnqueueObservable(call as SpCall<Resp>).singleElement()
            Call::class.java -> call
            else -> call.execute()
        } as Return
    }

}

private class SpEnqueueObservable<T>(private val originalCall: SpCall<T>) : Observable<T>() {

    override fun subscribeActual(subscriber: Observer<in T>) {
        val call = originalCall.clone()
        val callback = CallCallback(call, subscriber)
        subscriber.onSubscribe(callback)

        if (!callback.isDisposed) {
            call.enqueue(callback)
        }
    }

    private class CallCallback<T> internal constructor(
        private val call: SpCall<T>,
        private val observer: Observer<in T>
    ) : Disposable, Callback<T>, SharedPreferences.OnSharedPreferenceChangeListener {

        @Volatile
        private var disposed: Boolean = false

        init { call.proxy.registerOnSharedPreferenceChangeListener(this) }

        override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
            if (key == call.key && !disposed) {
                Schedulers.io().scheduleDirect {
                    call.callEnqueue(this@CallCallback)
                }
            }
        }

        override fun callSuccess(response: T) {
            callNext(response)
        }

        override fun callFail(t: Throwable) {
            if (disposed) return

            try {
                observer.onError(t)
            } catch (inner: Throwable) {
                Exceptions.throwIfFatal(inner)
                RxJavaPlugins.onError(CompositeException(t, inner))
            }
        }

        override fun dispose() {
            disposed = true
            call.cancel()
            call.proxy.unregisterOnSharedPreferenceChangeListener(this)
        }

        override fun isDisposed(): Boolean {
            return disposed
        }

        private fun callNext(response: T) {
            if (disposed) return

            if (response == null) {
                return
            }
            try {
                observer.onNext(response)
            } catch (t: Throwable) {
                if (!disposed) {
                    try {
                        observer.onError(t)
                    } catch (inner: Throwable) {
                        Exceptions.throwIfFatal(inner)
                        RxJavaPlugins.onError(CompositeException(t, inner))
                    }

                }
            }
        }
    }
}
